#!/bin/sh
kontena vault update -u FRONT_SERVICE_VERSION $KONTENA_FRONT_SERVICE_VERSION
kontena vault update -u APP_SERVICE_VERSION $KONTENA_APP_SERVICE_VERSION
kontena vault update -u AUTH_SERVICE_VERSION $KONTENA_AUTH_SERVICE_VERSION
if kontena stack list | grep -q article-project ; then kontena stack remove --force article-project ; fi
sleep 10
kontena stack install ./descriptors/kontena-ci-areas.yml
