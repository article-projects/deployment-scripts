#!/bin/sh
kontena vault update -u FRONT_SERVICE_VERSION $KONTENA_FRONT_SERVICE_VERSION
kontena vault update -u APP_SERVICE_VERSION $KONTENA_APP_SERVICE_VERSION
kontena vault update -u AUTH_SERVICE_VERSION $KONTENA_AUTH_SERVICE_VERSION
if kontena stack list | grep -q article-project ; then kontena stack upgrade article-project ./descriptors/kontena-test-areas.yml ; fi
if ! kontena stack list | grep -q article-project ; then kontena stack install ./descriptors/kontena-test-areas.yml ; fi
